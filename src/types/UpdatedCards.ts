export type UpdatedCards = {
    repoUrl: string;
    owner: { login: string; avatar_url: string };
    comments: { id: number; text: string }[];
    stargazers_count: number;
    avatarUrl: string;
    author: string;
    title: string;
    stargazersCount: number;
    watchersCount: number;
    html_url: string;
    name: string;
    id: number;
    watchers_count: number;
}

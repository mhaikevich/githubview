export enum LocalStorageKeys {
  PAGINATE_POSITION = "PAGINATE_POSITION",
  SEARCH = "SEARCH",
  CARDS = "CARDS",
  LIMIT = "LIMIT",
}

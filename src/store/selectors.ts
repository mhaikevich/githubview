import {IRepoState} from "../models/IRepo";

export const getCards = (appState: IRepoState) => {
    return appState.items;
};
export const getTotalCount = (appState: IRepoState) => {
    return appState.total_count;
};
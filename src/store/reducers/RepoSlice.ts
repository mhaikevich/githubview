import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IRepoState } from "../../models/IRepo";

import {
  fetchRepo,
  recoverRepo,
  setPageAction,
  setPageLimitAction,
} from "./ActionCreators";

const initialState: IRepoState = {
  total_count: 0, // "message": "Only the first 1000 search results are available",
  isLoading: false,
  error: "",
  items: [],
  currentPage: 1,
  limit: 10,
};

export const repoSlice = createSlice({
  name: "repo",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchRepo.fulfilled.type]: (state, action: PayloadAction<IRepoState>) => {
      state.isLoading = false;
      state.error = "";
      state.items = action.payload.items;
      state.total_count =
        action.payload.total_count > 1000
          ? (action.payload.total_count = 1000)
          : action.payload.total_count;
    },
    [fetchRepo.pending.type]: (state) => {
      state.isLoading = true;
    },
    [fetchRepo.rejected.type]: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    [recoverRepo.type]: (state, action: PayloadAction<IRepoState>) => {
      state.items = action.payload.items;
      state.total_count =
        action.payload.total_count > 1000
          ? (action.payload.total_count = 1000)
          : action.payload.total_count;
    },
    [setPageAction.type]: (state, action: PayloadAction<number>) => {
      state.currentPage = action.payload;
    },
    [setPageLimitAction.type]: (state, action: PayloadAction<number>) => {
      state.limit = action.payload;
    },
  },
});

export default repoSlice.reducer;

import {createAction, createAsyncThunk} from "@reduxjs/toolkit";
import axios from "axios";

import {LocalStorageKeys} from "../../types";
import {LocalStorageManager} from "../../services/local-storage-manager";
import {UpdatedCards} from "../../types/UpdatedCards";
import {IRepoState} from "../../models/IRepo";
import {IItems} from "../../models/IItems";

import {getCards, getTotalCount} from "../selectors";

export const recoverRepo = createAction<{ total_count: number, items: UpdatedCards[] }>(
    "RECOVER_REPO"
);
export const setPageAction = createAction<number>("SET_PAGE");
export const setPageLimitAction = createAction<number>("SET_PAGE_LIMIT");

export const fetchRepo = createAsyncThunk(
    "repo/fetchAll",
    async (
        repoData: { subject: string; limit: number; page: number },
        {rejectWithValue}
    ) => {
        const {subject, limit = 10, page = 1} = repoData;
        try {
            const response = await axios.get<IRepoState>(
                `https://api.github.com/search/repositories?q=${subject}&per_page=${limit}&page=${page + 1}`
            );
            LocalStorageManager.setItem(LocalStorageKeys.CARDS, {
                ...response.data,
                items: response.data.items.map((item: IItems) => {
                    return {...item, comments: []};
                }),
            });
            return response.data;
        } catch (e) {
            return rejectWithValue("Не удалось загрузить репозитории");
        }
    }
);

export const setPageThunk = createAsyncThunk(
    "setPage",
    async (
        repoData: { subject: string; limit: number; page: number },
        {rejectWithValue, dispatch}
    ) => {
        try {
            const {subject, limit = 10, page = 1} = repoData;
            dispatch(setPageAction(page));
            dispatch(fetchRepo({subject, limit, page}));
        } catch (e) {
            return rejectWithValue("Не удалось загрузить страницу");
        }
    }
);

export const setCommentThunk = createAsyncThunk(
    "setComment",
    async (
        commentData: { id: number; text: string },
        {dispatch, getState, rejectWithValue}: any
    ) => {
        try {
            const {id, text} = commentData;
            const totalCount = getTotalCount(getState().repoReducer)
            const cards = getCards(getState().repoReducer);
            const currentCardIndex = cards.findIndex(
                (card: { id: number }) => card.id === id
            );
            const newComment = {
                id: Date.now(),
                text,
            };
            const updatedCards: UpdatedCards[] = [
                ...cards.slice(0, currentCardIndex),
                {
                    ...cards[currentCardIndex],
                    comments: [...(cards[currentCardIndex]?.comments ?? []), newComment],
                },
                ...cards.slice(currentCardIndex + 1),
            ];
            dispatch(recoverRepo({total_count: totalCount, items: updatedCards}));
            await LocalStorageManager.setItem(LocalStorageKeys.CARDS, {
                total_count: totalCount,
                items: updatedCards,
            });
        } catch (e) {
            return rejectWithValue("Не удалось создать комментарий");
        }
    }
);

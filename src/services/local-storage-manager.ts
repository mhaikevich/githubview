import { LocalStorageKeys } from "../types";

type JSONLike =
    | { [property: string]: JSONLike }
    | readonly JSONLike[]
    | string
    | number
    | boolean
    | null;

export class LocalStorageManager {
  static setItem = async (key: LocalStorageKeys, value: JSONLike) => {
    await localStorage.setItem(key, JSON.stringify(value));
  };
  static getItem = async (key: LocalStorageKeys) => {
    if (localStorage.length) {
      const cachedItem = await localStorage.getItem(key);
      return cachedItem ? JSON.parse(cachedItem) : null;
    }
    return null;
  };
  static removeItem = async (key: LocalStorageKeys) => {
    await localStorage.removeItem(key);
  };
}

import React, { useState } from "react";

import SearchHeader from "./components/SearchHeader/SearchHeader";
import RepositoryContainer from "./components/RepositoryContainer/RepositoryContainer";
import FooterContainer from "./components/FooterContainer/FooterContainer";

import cl from "./App.module.scss";

const App = () => {
  const [searchValue, setSearchValue] = useState("");

  return (
    <div className={cl.appContainer}>
      <SearchHeader searchValue={searchValue} setSearchValue={setSearchValue} />
      <RepositoryContainer />
      <FooterContainer searchValue={searchValue} />
    </div>
  );
};

export default App;

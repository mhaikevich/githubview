import React, { FC, useEffect } from "react";

import Select from "./Select/Select";
import Pagination from "./Pagination/Pagination";
import { useAppDispatch, useAppSelector } from "../../hooks/redux";
import { LocalStorageKeys } from "../../types";
import { LocalStorageManager } from "../../services/local-storage-manager";
import {
  fetchRepo,
  setPageLimitAction,
} from "../../store/reducers/ActionCreators";

import cl from "./style.module.scss";

const optionsData = [
  { value: 10, name: "10" },
  { value: 25, name: "25" },
  { value: 50, name: "50" },
];

type FooterContainerType = {
  searchValue: string;
};

const FooterContainer: FC<FooterContainerType> = ({ searchValue }) => {
  const dispatch = useAppDispatch();

  const { isLoading, limit, currentPage, items } = useAppSelector(
    (state) => state.repoReducer
  );

  useEffect(() => {
    void (async () => {
      const limit = await LocalStorageManager.getItem(LocalStorageKeys.LIMIT);
      if (limit) dispatch(setPageLimitAction(limit));
    })();
  }, [dispatch]);

  const sortRepo = (sort: number) => {
    dispatch(setPageLimitAction(sort));
    dispatch(
      fetchRepo({ subject: searchValue, limit: sort, page: currentPage })
    );
    LocalStorageManager.setItem(LocalStorageKeys.LIMIT, sort);
  };

  const isShowOnInitial = items.length > 0;
  return (
    <footer className={`${cl.footerWrapper} d-flex align-items-center`}>
      {!isLoading && isShowOnInitial ? (
        <Select value={limit} onChange={sortRepo} options={optionsData} />
      ) : null}
      {!isLoading && isShowOnInitial ? <Pagination /> : null}
    </footer>
  );
};

export default FooterContainer;

import React, {useEffect} from "react";
import ReactPaginate from "react-paginate";

import {LocalStorageKeys} from "../../../types";
import {useAppDispatch, useAppSelector} from "../../../hooks/redux";
import {
    setPageAction,
    setPageThunk,
} from "../../../store/reducers/ActionCreators";
import {LocalStorageManager} from "../../../services/local-storage-manager";

import "./style.scss";

const Pagination = () => {
    const dispatch = useAppDispatch();
    const {total_count, currentPage, limit} = useAppSelector(
        (state) => state.repoReducer
    );

    useEffect(() => {
        LocalStorageManager.getItem(LocalStorageKeys.PAGINATE_POSITION).then(
            (paginatePosition) => {
                dispatch(setPageAction(paginatePosition));
            }
        );
    }, [dispatch]);

    const onPageChange = async ({selected}: { selected: number }) => {
        const searchValue = await LocalStorageManager.getItem(
            LocalStorageKeys.SEARCH
        );
        dispatch(setPageThunk({subject: searchValue, page: selected, limit}));
        await LocalStorageManager.setItem(
            LocalStorageKeys.PAGINATE_POSITION,
            selected
        );
    };

    return (
        <ReactPaginate
            forcePage={currentPage}
            breakLabel="..."
            nextLabel={<span className="arrow arrow-right"/>}
            onPageChange={onPageChange}
            pageRangeDisplayed={3}
            pageCount={Math.ceil(total_count / limit)}
            previousLabel={<span className="arrow arrow-left"/>}
            activeClassName={`active`}
            breakClassName={`item break-me`}
            containerClassName={`pagination`}
            disabledClassName={`disabled-page`}
            nextClassName={`item next`}
            pageClassName={`item pagination-page`}
            previousClassName={`item previous`}
        />
    );
};

export default Pagination;

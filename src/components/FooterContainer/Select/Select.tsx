import React, { FC } from "react";

import cl from "./style.module.scss";

type SelectOptionType = {
  value: number;
  name: string;
};

type SelectType = {
  options: SelectOptionType[];
  value: number;
  onChange: (value: number) => void;
};

const Select: FC<SelectType> = ({ options, value, onChange }) => {
  return (
    <div className={`${cl.selectWrapper}`}>
      <select value={value} onChange={(event) => onChange(+event.target.value)}>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.name}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;

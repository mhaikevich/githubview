import React, {FC, useState} from "react";

import {useAppDispatch, useAppSelector} from "../../../hooks/redux";
import {setCommentThunk} from "../../../store/reducers/ActionCreators";

import cl from "./style.module.scss";

type RepositoryType = {
    id: number;
    title: string;
    author: string;
    stargazersCount: number;
    watchersCount: number;
    avatarUrl: string;
    repoUrl: string;
};

const Repository: FC<RepositoryType> = ({
                                            id,
                                            title,
                                            author,
                                            stargazersCount,
                                            watchersCount,
                                            avatarUrl,
                                            repoUrl,
                                        }) => {
    const dispatch = useAppDispatch();
    const {items} = useAppSelector((state) => state.repoReducer);

    const [commentValue, setCommentValue] = useState("");

    const currentRepo = items.find((repo: any) => repo.id === id);

    const addComment = () => {
        dispatch(setCommentThunk({id, text: commentValue}));
        setCommentValue("");
    };

    const commentWrapperHandler = (e: React.MouseEvent<HTMLDivElement>) => {
        e.stopPropagation();
        e.preventDefault();
    };

    const inputCommentOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCommentValue(e.target.value);
    };

    return (
        <a
            className={`${cl.repository}`}
            href={repoUrl}
            rel="noreferrer"
            target="_blank"
        >
            <h1 className={cl.title}>{title}</h1>
            <div className={`${cl.authorWrapper} d-flex align-items-center `}>
                <img className={cl.authorImage} src={avatarUrl} alt="authorAvatar"/>
                <h2 className={cl.authorUsername}>{author}</h2>
            </div>
            <div className={`${cl.infoWrapper} d-flex align-items-center`}>
                <div className={`${cl.stargazers} d-flex align-items-center`}>
                    <div className={cl.star}>
                        <span/>
                    </div>
                    <div className={cl.stargazersCount}>{stargazersCount}</div>
                </div>

                <div className={`${cl.watchers} d-flex align-items-center`}>
                    <span className={cl.eye}></span>
                    <div className={cl.watchersCount}>{watchersCount}</div>
                </div>
            </div>
            <div
                onClick={commentWrapperHandler}
                className={`${cl.commentWrapper}  d-flex`}
            >
                <input
                    type="text"
                    placeholder="Комментарий к проекту"
                    value={commentValue}
                    onChange={inputCommentOnChange}
                />
                <button
                    onClick={addComment}
                    disabled={!commentValue}
                    className={`${cl.enterCommentBtn} d-flex align-items-center justify-content-center btn btn-primary btn-lg`}
                >
                    <div className={cl.pencil}></div>
                </button>
            </div>
            {currentRepo?.comments?.map((comment: { id: number; text: string }) => (
                <p className={cl.commentItem} key={comment.id}>
                    {comment.text}
                </p>
            ))}
        </a>
    );
};

export default Repository;

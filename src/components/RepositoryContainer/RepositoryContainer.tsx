import React, {useEffect} from "react";

import Repository from "./Repository/Repository";
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {LocalStorageKeys} from "../../types";
import {LocalStorageManager} from "../../services/local-storage-manager";
import {recoverRepo} from "../../store/reducers/ActionCreators";

import cl from "./style.module.scss";

const RepositoryContainer = () => {
    const dispatch = useAppDispatch();
    const {items, isLoading, error} = useAppSelector(
        (state) => state.repoReducer
    );

    useEffect(() => {
        LocalStorageManager.getItem(LocalStorageKeys.CARDS).then((cardsArray) => {
            if (cardsArray) {
                dispatch(recoverRepo({total_count: cardsArray.total_count, items: cardsArray.items}));
            }
        });
    }, [dispatch]);

    return (
        <section className={`${cl.repositoryContainer}`}>
            {isLoading ? <h1 className={cl.loading}>Поиск проектов...</h1> : null}
            {error ? <h1 className={cl.loading}>{error}</h1> : null}
            {!isLoading
                ? items.map((repo: any) => {
                    return (
                        <Repository
                            key={repo.id}
                            id={repo.id}
                            repoUrl={repo.html_url}
                            title={repo.name}
                            author={repo.owner.login}
                            stargazersCount={repo.stargazers_count}
                            watchersCount={repo.watchers_count}
                            avatarUrl={repo.owner.avatar_url}
                        />
                    );
                })
                : null}
        </section>
    );
};

export default RepositoryContainer;

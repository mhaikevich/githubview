import React, { FC, useCallback, useEffect } from "react";

import { LocalStorageKeys } from "../../types";
import { useAppDispatch, useAppSelector } from "../../hooks/redux";
import { LocalStorageManager } from "../../services/local-storage-manager";
import {
  setPageLimitAction,
  setPageThunk,
} from "../../store/reducers/ActionCreators";
import SearchIcon from "../../assets/SVG/SearchIcon";

import cl from "./style.module.scss";

type SearchHeaderType = {
  searchValue: string;
  setSearchValue: (value: string) => void;
};

const SearchHeader: FC<SearchHeaderType> = ({
  searchValue,
  setSearchValue,
}) => {
  const dispatch = useAppDispatch();
  const { limit } = useAppSelector((state) => state.repoReducer);

  const searchHandler = useCallback(
    (value: string) => {
      dispatch(setPageLimitAction(10));
      dispatch(setPageThunk({ subject: value, page: 0, limit }));
      LocalStorageManager.setItem(LocalStorageKeys.PAGINATE_POSITION, 0);
    },
    [dispatch, limit]
  );

  const handleSearchInput = useCallback(
    async (e: React.ChangeEvent<HTMLInputElement>) => {
      setSearchValue(e.target.value);
      await LocalStorageManager.setItem(
        LocalStorageKeys.SEARCH,
        e.target.value
      );
    },
    [setSearchValue]
  );

  useEffect(() => {
    void (async () => {
      const cachedSearchValue = await LocalStorageManager.getItem(
        LocalStorageKeys.SEARCH
      );
      if (cachedSearchValue) setSearchValue(cachedSearchValue);
    })();
  }, [searchHandler, setSearchValue]);

  return (
    <header
      className={`${cl.headerWrapper} d-flex justify-content-center align-items-center`}
    >
      <div className={`${cl.headerWrapper} d-flex`}>
        <input
          className={cl.searchInput}
          type="text"
          placeholder="Начните вводить текст для поиска (не менее трех символов)"
          value={searchValue}
          onChange={handleSearchInput}
        />
        <button
          type="button"
          onClick={() => searchHandler(searchValue)}
          className={`${cl.searchBtn} btn btn-primary btn-lg`}
        >
          <SearchIcon />
        </button>
      </div>
    </header>
  );
};

export default SearchHeader;

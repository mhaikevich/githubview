import {IItems} from "./IItems";

export interface IRepoState {
    total_count: number;
    isLoading: boolean;
    error: string;
    items: IItems[];
    currentPage: number;
    limit: number;
}

export interface IItems {
  id: number;
  title: string;
  author: string;
  stargazersCount: number;
  watchersCount: number;
  avatarUrl: string;
  repoUrl: string;
  comments: [{ id: number; text: string }];
  html_url: string;
  name: string;
  owner: { login: string; avatar_url: string };
  stargazers_count: number;
  watchers_count: number;
}